package com.arberthaci.neugelbmovies.base

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.mockk.MockKAnnotations
import org.junit.Before
import org.junit.ClassRule
import org.junit.Rule

/**
 * Created by Arbër Thaçi on 2021-10-01.
 * Email: arberlthaci@gmail.com
 */

open class BaseTestClass {

    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    companion object {
        @ClassRule
        @JvmField
        val schedulers = RxImmediateSchedulerRule()
    }

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true) // turn relaxUnitFun on for all mocks
    }
}
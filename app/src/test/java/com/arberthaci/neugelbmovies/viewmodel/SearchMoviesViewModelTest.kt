package com.arberthaci.neugelbmovies.viewmodel

import androidx.lifecycle.Observer
import com.arberthaci.neugelbmovies.base.BaseTestClass
import com.arberthaci.neugelbmovies.common.livedata.Outcome
import com.arberthaci.neugelbmovies.data.model.Movie
import com.arberthaci.neugelbmovies.data.model.RxMovieResponse
import com.arberthaci.neugelbmovies.data.repository.movies.MoviesRepository
import com.arberthaci.neugelbmovies.data.repository.movies.exceptions.MoviesException
import com.arberthaci.neugelbmovies.ui.movies.view.SearchMoviesFragmentDirections
import com.arberthaci.neugelbmovies.ui.movies.viewmodel.SearchMoviesViewModel
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.spyk
import io.mockk.verify
import io.reactivex.Observable
import org.junit.Test

/**
 * Created by Arbër Thaçi on 2021-10-01.
 * Email: arberlthaci@gmail.com
 */

class SearchMoviesViewModelTest : BaseTestClass() {

    companion object {
        private const val FIRST_PAGE = 1
        private val NEXT_PAGE = (2..999).random()
        private const val QUERY = "query"

        private val moviesResponse = listOf(
            Movie(id = 1),
            Movie(id = 2),
            Movie(id = 3)
        )
    }

    @MockK
    lateinit var moviesRepository: MoviesRepository

    @MockK
    lateinit var searchMoviesOutcome: Observer<Outcome<RxMovieResponse>>

    private val searchMoviesViewModel: SearchMoviesViewModel by lazy {
        spyk(
            SearchMoviesViewModel(
                moviesRepository = moviesRepository
            )
        )
    }

    @Test
    fun test_getLastQuery() {
        searchMoviesViewModel.searchMovies(
            query = QUERY,
            page = FIRST_PAGE
        )

        assert(searchMoviesViewModel.getLastQuery() == QUERY)
    }

    @Test
    fun test_searchMovies_firstPage_success() {
        searchMoviesViewModel.searchMoviesOutcome.observeForever(searchMoviesOutcome)

        every {
            moviesRepository.searchMovies(
                query = QUERY,
                page = FIRST_PAGE
            )
        } returns Observable.just(moviesResponse)

        searchMoviesViewModel.configureSearchMovies()
        searchMoviesViewModel.searchMovies(
            query = QUERY,
            page = FIRST_PAGE
        )

        verify {
            searchMoviesOutcome.onChanged(
                Outcome.success(
                    RxMovieResponse.FirstPage(
                        movies = moviesResponse
                    )
                )
            )
        }
    }

    @Test
    fun test_searchMovies_nextPage_success() {
        searchMoviesViewModel.searchMoviesOutcome.observeForever(searchMoviesOutcome)

        every {
            moviesRepository.searchMovies(
                query = QUERY,
                page = NEXT_PAGE
            )
        } returns Observable.just(moviesResponse)

        searchMoviesViewModel.configureSearchMovies()
        searchMoviesViewModel.searchMovies(
            query = QUERY,
            page = NEXT_PAGE
        )

        verify {
            searchMoviesOutcome.onChanged(
                Outcome.success(
                    RxMovieResponse.NextPage(
                        movies = moviesResponse
                    )
                )
            )
        }
    }

    @Test
    fun test_searchMovies_firstPage_error() {
        val throwable = MoviesException.InvalidData

        searchMoviesViewModel.searchMoviesOutcome.observeForever(searchMoviesOutcome)

        every {
            moviesRepository.searchMovies(
                query = QUERY,
                page = FIRST_PAGE
            )
        } returns Observable.error(throwable)

        searchMoviesViewModel.configureSearchMovies()
        searchMoviesViewModel.searchMovies(
            query = QUERY,
            page = FIRST_PAGE
        )

        verify { searchMoviesOutcome.onChanged(Outcome.failure(throwable)) }
    }

    @Test
    fun test_resetSearchMovies() {
        searchMoviesViewModel.searchMoviesOutcome.observeForever(searchMoviesOutcome)

        searchMoviesViewModel.configureSearchMovies()
        searchMoviesViewModel.resetSearchMovies()

        verify { searchMoviesOutcome.onChanged(Outcome.success(RxMovieResponse.Restart)) }
        assert(searchMoviesViewModel.getLastQuery() == "")
    }

    @Test
    fun test_navigateToMovieDetailsFragment() {
        searchMoviesViewModel.navigateToMovieDetailsFragment(
            movie = moviesResponse.first()
        )

        verify {
            searchMoviesViewModel.navigate(
                SearchMoviesFragmentDirections.openMovieDetailsFragment(
                    moviesResponse.first()
                )
            )
        }
    }
}
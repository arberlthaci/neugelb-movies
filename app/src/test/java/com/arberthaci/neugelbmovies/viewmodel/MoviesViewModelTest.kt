package com.arberthaci.neugelbmovies.viewmodel

import androidx.lifecycle.Observer
import com.arberthaci.neugelbmovies.base.BaseTestClass
import com.arberthaci.neugelbmovies.common.livedata.Outcome
import com.arberthaci.neugelbmovies.data.model.Movie
import com.arberthaci.neugelbmovies.data.model.RxMovieResponse
import com.arberthaci.neugelbmovies.data.repository.movies.MoviesRepository
import com.arberthaci.neugelbmovies.data.repository.movies.exceptions.MoviesException
import com.arberthaci.neugelbmovies.ui.movies.view.MoviesFragmentDirections
import com.arberthaci.neugelbmovies.ui.movies.viewmodel.MoviesViewModel
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.spyk
import io.mockk.verify
import io.reactivex.Observable
import org.junit.Test

/**
 * Created by Arbër Thaçi on 2021-10-01.
 * Email: arberlthaci@gmail.com
 */

class MoviesViewModelTest : BaseTestClass() {

    companion object {
        private const val FIRST_PAGE = 1
        private val NEXT_PAGE = (2..999).random()

        private val moviesResponse = listOf(
            Movie(id = 1),
            Movie(id = 2),
            Movie(id = 3)
        )
    }

    @MockK
    lateinit var moviesRepository: MoviesRepository

    @MockK
    lateinit var moviesOutcome: Observer<Outcome<RxMovieResponse>>

    private val moviesViewModel: MoviesViewModel by lazy {
        spyk(
            MoviesViewModel(
                moviesRepository = moviesRepository
            )
        )
    }

    @Test
    fun test_getLatestMovies_firstPage_success() {
        moviesViewModel.moviesOutcome.observeForever(moviesOutcome)

        every {
            moviesRepository.getLatestMovies(
                page = FIRST_PAGE
            )
        } returns Observable.just(moviesResponse)

        moviesViewModel.getLatestMovies(
            page = FIRST_PAGE
        )

        verify {
            moviesOutcome.onChanged(
                Outcome.success(
                    RxMovieResponse.FirstPage(
                        movies = moviesResponse
                    )
                )
            )
        }
    }

    @Test
    fun test_getLatestMovies_nextPage_success() {
        moviesViewModel.moviesOutcome.observeForever(moviesOutcome)

        every {
            moviesRepository.getLatestMovies(
                page = NEXT_PAGE
            )
        } returns Observable.just(moviesResponse)

        moviesViewModel.getLatestMovies(
            page = NEXT_PAGE
        )

        verify {
            moviesOutcome.onChanged(
                Outcome.success(
                    RxMovieResponse.NextPage(
                        movies = moviesResponse
                    )
                )
            )
        }
    }

    @Test
    fun test_getLatestMovies_firstPage_error() {
        val throwable = MoviesException.InvalidData

        moviesViewModel.moviesOutcome.observeForever(moviesOutcome)

        every {
            moviesRepository.getLatestMovies(
                page = FIRST_PAGE
            )
        } returns Observable.error(throwable)

        moviesViewModel.getLatestMovies(
            page = FIRST_PAGE
        )

        verify { moviesOutcome.onChanged(Outcome.failure(throwable)) }
    }

    @Test
    fun test_navigateToSearchMoviesFragment() {
        moviesViewModel.navigateToSearchMoviesFragment()

        verify {
            moviesViewModel.navigate(
                MoviesFragmentDirections.openSearchMoviesFragment()
            )
        }
    }

    @Test
    fun test_navigateToMovieDetailsFragment() {
        moviesViewModel.navigateToMovieDetailsFragment(
            movie = moviesResponse.first()
        )

        verify {
            moviesViewModel.navigate(
                MoviesFragmentDirections.openMovieDetailsFragment(
                    moviesResponse.first()
                )
            )
        }
    }
}
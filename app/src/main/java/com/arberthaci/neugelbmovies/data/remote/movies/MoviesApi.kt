package com.arberthaci.neugelbmovies.data.remote.movies

import com.arberthaci.neugelbmovies.data.model.MoviesResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.QueryMap

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

interface MoviesApi {

    @GET("discover/movie")
    fun getLatestMovies(
        @QueryMap filterParams: Map<String, String>
    ): Observable<MoviesResponse>

    @GET("search/movie")
    fun searchMovies(
        @QueryMap filterParams: Map<String, String>
    ): Observable<MoviesResponse>
}
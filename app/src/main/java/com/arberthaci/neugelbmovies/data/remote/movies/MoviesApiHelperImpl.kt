package com.arberthaci.neugelbmovies.data.remote.movies

import com.arberthaci.neugelbmovies.data.model.MoviesResponse
import com.arberthaci.neugelbmovies.data.remote.Network
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

class MoviesApiHelperImpl @Inject constructor(
    private val network: Network
) : MoviesApiHelper {

    private val moviesApi by lazy {
        network.getRetrofit().create(MoviesApi::class.java)
    }

    override fun getLatestMovies(
        filterParams: Map<String, String>
    ): Observable<MoviesResponse> = moviesApi.getLatestMovies(
        filterParams = filterParams
    )

    override fun searchMovies(filterParams: Map<String, String>) = moviesApi.searchMovies(
        filterParams = filterParams
    )
}
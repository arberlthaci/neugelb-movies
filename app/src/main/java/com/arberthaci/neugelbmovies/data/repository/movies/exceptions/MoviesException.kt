package com.arberthaci.neugelbmovies.data.repository.movies.exceptions

import com.arberthaci.neugelbmovies.common.exceptions.NeugelbException

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

sealed class MoviesException : NeugelbException.FeatureException() {
    object NoResults : MoviesException()
    object InvalidData : MoviesException()
}
package com.arberthaci.neugelbmovies.data.model.constants

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

object Constants {
    const val IMAGE_BASE_URL = "https://image.tmdb.org/t/p"
    const val IMAGE_FILE_SIZE_W500 = "/w500"
    const val IMAGE_FILE_SIZE_ORIGINAL = "/original"
    const val PARAM_API_KEY = "api_key"
    const val PARAM_QUERY = "query"
    const val PARAM_PAGE = "page"
    const val PARAM_SORT_BY = "sort_by"
    const val PARAM_SORT_BY_VALUE = "release_date.desc"
    const val PARAM_RELEASE_DATE_LTE = "release_date.lte"
    const val PARAM_INCLUDE_ADULT = "include_adult"
    const val DATE_FORMAT = "yyyy-MM-dd"
}
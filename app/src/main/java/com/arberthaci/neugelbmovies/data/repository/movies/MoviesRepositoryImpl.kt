package com.arberthaci.neugelbmovies.data.repository.movies

import com.arberthaci.neugelbmovies.common.exceptions.NeugelbException
import com.arberthaci.neugelbmovies.common.extensions.swapException
import com.arberthaci.neugelbmovies.common.utils.InternetUtils
import com.arberthaci.neugelbmovies.data.model.Movie
import com.arberthaci.neugelbmovies.data.model.constants.Constants
import com.arberthaci.neugelbmovies.data.remote.movies.MoviesApiHelper
import com.arberthaci.neugelbmovies.data.repository.movies.exceptions.MoviesException
import io.reactivex.Observable
import io.reactivex.exceptions.Exceptions
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import kotlin.collections.HashMap

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

class MoviesRepositoryImpl(
    private val apiHelper: MoviesApiHelper
) : MoviesRepository {

    override fun getLatestMovies(page: Int): Observable<List<Movie>> {
        val filterParams: HashMap<String, String> = hashMapOf()
        filterParams[Constants.PARAM_PAGE] = page.toString()
        filterParams[Constants.PARAM_SORT_BY] = Constants.PARAM_SORT_BY_VALUE
        filterParams[Constants.PARAM_RELEASE_DATE_LTE] = SimpleDateFormat(
            Constants.DATE_FORMAT, Locale.getDefault()
        ).format(Date())
        filterParams[Constants.PARAM_INCLUDE_ADULT] = false.toString()

        return Observable.fromCallable {
            (InternetUtils.isInternetOn())
        }.flatMap { hasConnection ->
            if (hasConnection) {
                apiHelper.getLatestMovies(
                    filterParams = filterParams
                ).map {
                    when {
                        it.results.isNotEmpty() -> {
                            return@map it.results
                        }
                        page > 1 -> {
                            return@map listOf()
                        }
                        page == 1 -> {
                            throw Exceptions.propagate(MoviesException.NoResults)
                        }
                        else -> {
                            throw Exceptions.propagate(MoviesException.InvalidData)
                        }
                    }
                }
            } else {
                Observable.error(NeugelbException.NoConnection)
            }
        }.swapException(NeugelbException.ServerError)
    }

    override fun searchMovies(query: String, page: Int): Observable<List<Movie>> {
        val filterParams: HashMap<String, String> = hashMapOf()
        filterParams[Constants.PARAM_QUERY] = query
        filterParams[Constants.PARAM_PAGE] = page.toString()
        filterParams[Constants.PARAM_INCLUDE_ADULT] = false.toString()

        return Observable.fromCallable {
            (InternetUtils.isInternetOn())
        }.flatMap { hasConnection ->
            if (hasConnection) {
                apiHelper.searchMovies(
                    filterParams = filterParams
                ).map {
                    when {
                        it.results.isNotEmpty() -> {
                            return@map it.results
                        }
                        page > 1 -> {
                            return@map listOf()
                        }
                        page == 1 -> {
                            throw Exceptions.propagate(MoviesException.NoResults)
                        }
                        else -> {
                            throw Exceptions.propagate(MoviesException.InvalidData)
                        }
                    }
                }
            } else {
                Observable.error(NeugelbException.NoConnection)
            }
        }.swapException(NeugelbException.ServerError)
    }
}
package com.arberthaci.neugelbmovies.data.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

data class MoviesResponse(
    @SerializedName("page")
    val page: Int?,
    @SerializedName("results")
    val results: List<Movie> = listOf(),
    @SerializedName("total_pages")
    val totalPages: Int?,
    @SerializedName("total_results")
    val totalResults: Int?
)
package com.arberthaci.neugelbmovies.data.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

data class Movie(
    @SerializedName("poster_path")
    val posterPath: String?,
    @SerializedName("overview")
    val overview: String,
    @SerializedName("release_date")
    val releaseDate: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("title")
    val title: String,
    @SerializedName("vote_average")
    val voteAverage: Double,
    var displayMode: MovieDisplayMode? = null
) : Serializable {

    constructor(id: Int) : this (
        "",
        "",
        "",
        id,
        "",
        0.0
    )
}
package com.arberthaci.neugelbmovies.data.model

/**
 * Created by Arbër Thaçi on 2021-09-30.
 * Email: arberlthaci@gmail.com
 */

enum class MovieDisplayMode {
    GRID,
    LINEAR
}
package com.arberthaci.neugelbmovies.data.remote.movies

import com.arberthaci.neugelbmovies.data.model.MoviesResponse
import io.reactivex.Observable

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

interface MoviesApiHelper {

    fun getLatestMovies(
        filterParams: Map<String, String>
    ): Observable<MoviesResponse>

    fun searchMovies(
        filterParams: Map<String, String>
    ): Observable<MoviesResponse>
}
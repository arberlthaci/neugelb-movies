package com.arberthaci.neugelbmovies.data.remote

import com.arberthaci.neugelbmovies.BuildConfig
import com.arberthaci.neugelbmovies.data.model.constants.Constants
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

class NeugelbNetwork @Inject constructor() : Network {

    companion object {
        internal const val HEADER_CONTENT_TYPE_NAME = "Content-Type"
        internal const val HEADER_ACCEPT_NAME = "Accept"
        internal const val HEADER_APPLICATION_JSON_VALUE = "application/json"
        internal const val CONNECT_TIMEOUT = 30L
        internal const val WRITE_TIMEOUT = 30L
        internal const val READ_TIMEOUT = 90L
    }

    override fun getRetrofit(): Retrofit =
        Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(getHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

    private fun getHttpClient(): OkHttpClient {
        val httpClient = OkHttpClient.Builder()
            .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
            .addInterceptor { chain ->
                val original = chain.request()
                val originalHttpUrl = original.url()

                val url = originalHttpUrl.newBuilder()
                    .addQueryParameter(Constants.PARAM_API_KEY, BuildConfig.API_KEY)
                    .build()

                val requestBuilder = chain.request().newBuilder()
                    .addHeader(HEADER_CONTENT_TYPE_NAME, HEADER_APPLICATION_JSON_VALUE)
                    .addHeader(HEADER_ACCEPT_NAME, HEADER_APPLICATION_JSON_VALUE)
                    .url(url)

                val request = requestBuilder.build()
                chain.proceed(request)
            }

        if (BuildConfig.DEBUG) {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY

            httpClient.addInterceptor(logging)
        }

        return httpClient.build()
    }
}
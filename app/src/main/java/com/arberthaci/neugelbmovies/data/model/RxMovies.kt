package com.arberthaci.neugelbmovies.data.model

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

sealed class RxMovieRequest {

    data class NewRequest(
        val query: String,
        val page: Int = 1
    ) : RxMovieRequest()
    object Cancel : RxMovieRequest()
}

sealed class RxMovieResponse {

    data class FirstPage(val movies: List<Movie>) : RxMovieResponse()
    data class NextPage(val movies: List<Movie>) : RxMovieResponse()
    object Restart : RxMovieResponse()
}
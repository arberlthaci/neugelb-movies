package com.arberthaci.neugelbmovies.data.repository.movies

import com.arberthaci.neugelbmovies.data.model.Movie
import io.reactivex.Observable

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

interface MoviesRepository {

    fun getLatestMovies(page: Int): Observable<List<Movie>>

    fun searchMovies(query: String, page: Int): Observable<List<Movie>>
}
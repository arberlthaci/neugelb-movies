package com.arberthaci.neugelbmovies.data.remote

import retrofit2.Retrofit

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

interface Network {
    fun getRetrofit(): Retrofit
}
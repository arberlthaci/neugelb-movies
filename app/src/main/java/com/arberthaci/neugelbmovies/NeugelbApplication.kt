package com.arberthaci.neugelbmovies

import android.app.Application
import android.content.Context
import com.arberthaci.neugelbmovies.common.utils.InternetUtils
import com.arberthaci.neugelbmovies.di.component.AppComponent
import com.arberthaci.neugelbmovies.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import net.danlew.android.joda.JodaTimeAndroid
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

open class NeugelbApplication :
    Application(),
    HasAndroidInjector {

    companion object {
        lateinit var instance: NeugelbApplication

        operator fun get(context: Context): NeugelbApplication {
            return context.applicationContext as NeugelbApplication
        }
    }

    @Inject
    internal lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    lateinit var component: AppComponent

    override fun androidInjector(): AndroidInjector<Any> =
        dispatchingAndroidInjector

    override fun onCreate() {
        instance = this
        super.onCreate()

        // Init JodaTime
        JodaTimeAndroid.init(this)
        // Init InternetUtils
        InternetUtils.init(this)

        // Init Timber is the application is running in debug mode
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        inject()
    }

    open fun inject() {
        component = DaggerAppComponent.builder()
            .application(this)
            .build()
        component.inject(this)
    }
}
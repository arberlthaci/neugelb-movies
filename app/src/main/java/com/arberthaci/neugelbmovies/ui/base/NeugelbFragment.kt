package com.arberthaci.neugelbmovies.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

abstract class NeugelbFragment : BaseFragment() {

    private var mView: View? = null

    override fun enableBackButton() = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (mView == null) {
            mView = super.onCreateView(inflater, container, savedInstanceState)
        }
        return mView
    }
}
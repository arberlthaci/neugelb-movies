package com.arberthaci.neugelbmovies.ui.movies.adapter.delegates

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.arberthaci.neugelbmovies.R
import com.arberthaci.neugelbmovies.common.extensions.load
import com.arberthaci.neugelbmovies.common.extensions.visible
import com.arberthaci.neugelbmovies.common.ui.recyclerview.adapter.ViewTypeDelegateAdapter
import com.arberthaci.neugelbmovies.data.model.Movie
import com.arberthaci.neugelbmovies.ui.movies.adapter.callbacks.MovieCallback
import com.google.android.material.card.MaterialCardView

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

class MovieGridHolderDelegate(
    private val callback: MovieCallback
) : ViewTypeDelegateAdapter<Movie> {

    override fun onCreateViewHolder(parent: ViewGroup) =
        MovieGridViewHolder(
            itemLayoutView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_movie_grid, parent, false)
        )

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: Movie) {
        holder as MovieGridViewHolder
        holder.bind(
            item = item,
            callback = callback
        )
    }

    class MovieGridViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {

        private val mcvRoot by lazy { itemLayoutView.findViewById<MaterialCardView>(R.id.mcv_root) }
        private val vThumbnail by lazy { itemLayoutView.findViewById<View>(R.id.v_placeholder) }
        private val ivThumbnail by lazy { itemLayoutView.findViewById<ImageView>(R.id.iv_thumbnail) }
        private val tvTitle by lazy { itemLayoutView.findViewById<TextView>(R.id.tv_title) }

        fun bind(
            item: Movie,
            callback: MovieCallback
        ) {
            mcvRoot.setOnClickListener {
                callback.onMovieClicked(
                    movie = item
                )
            }

            ivThumbnail.load(
                url = item.posterPath,
                originalSize = false,
                onError = {
                    vThumbnail.visible()
                }
            )

            tvTitle.text = item.title
        }
    }
}
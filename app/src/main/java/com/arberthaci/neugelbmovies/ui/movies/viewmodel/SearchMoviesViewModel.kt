package com.arberthaci.neugelbmovies.ui.movies.viewmodel

import androidx.lifecycle.MutableLiveData
import com.arberthaci.neugelbmovies.common.livedata.Outcome
import com.arberthaci.neugelbmovies.common.livedata.extensions.failed
import com.arberthaci.neugelbmovies.common.livedata.extensions.loading
import com.arberthaci.neugelbmovies.common.livedata.extensions.success
import com.arberthaci.neugelbmovies.common.livedata.extensions.toMutableLiveData
import com.arberthaci.neugelbmovies.data.model.Movie
import com.arberthaci.neugelbmovies.data.model.MovieDisplayMode
import com.arberthaci.neugelbmovies.data.model.RxMovieRequest
import com.arberthaci.neugelbmovies.data.model.RxMovieResponse
import com.arberthaci.neugelbmovies.data.repository.movies.MoviesRepository
import com.arberthaci.neugelbmovies.ui.base.BaseViewModel
import com.arberthaci.neugelbmovies.ui.movies.view.SearchMoviesFragmentDirections
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 2021-09-30.
 * Email: arberlthaci@gmail.com
 */

class SearchMoviesViewModel @Inject constructor(
    private val moviesRepository: MoviesRepository
) : BaseViewModel() {

    private var lastQuery = ""

    private var rxMovieRequestPublishSubject: PublishSubject<RxMovieRequest>? = null

    private val searchMoviesSubject: PublishSubject<Outcome<RxMovieResponse>> =
        PublishSubject.create()
    val searchMoviesOutcome: MutableLiveData<Outcome<RxMovieResponse>> by lazy {
        searchMoviesSubject.toMutableLiveData(disposables)
    }

    fun getLastQuery() = lastQuery

    fun configureSearchMovies() {
        rxMovieRequestPublishSubject = PublishSubject.create()

        if (rxMovieRequestPublishSubject != null) {
            rxMovieRequestPublishSubject!!
                .debounce(300, TimeUnit.MILLISECONDS)
                .distinctUntilChanged()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { rxMovieRequest ->
                        when (rxMovieRequest) {
                            is RxMovieRequest.NewRequest -> {
                                callSearchMoviesApi(
                                    newRequest = rxMovieRequest
                                )
                            }
                            is RxMovieRequest.Cancel -> {
                                searchMoviesSubject.success(
                                    RxMovieResponse.Restart
                                )
                            }
                        }
                    },
                    {
                        searchMoviesSubject.failed(it)
                    }
                )
                .addTo(disposables)
        }
    }

    fun searchMovies(
        query: String,
        page: Int = 1
    ) {
        lastQuery = query
        rxMovieRequestPublishSubject?.onNext(
            RxMovieRequest.NewRequest(
                query = query,
                page = page
            )
        )
    }

    private fun callSearchMoviesApi(newRequest: RxMovieRequest.NewRequest) {
        moviesRepository.searchMovies(
            query = newRequest.query,
            page = newRequest.page
        )
            .takeUntil(rxMovieRequestPublishSubject)
            .map { movies ->
                movies.forEach { it.displayMode = MovieDisplayMode.LINEAR }

                return@map if (newRequest.page == 1) {
                    RxMovieResponse.FirstPage(
                        movies = movies
                    )
                } else {
                    RxMovieResponse.NextPage(
                        movies = movies
                    )
                }
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                if (newRequest.page == 1) {
                    searchMoviesSubject.loading(true)
                }
            }
            .subscribe(
                {
                    searchMoviesSubject.success(it)
                },
                {
                    searchMoviesSubject.failed(it)
                }
            )
            .addTo(disposables)
    }

    fun resetSearchMovies() {
        rxMovieRequestPublishSubject?.onNext(RxMovieRequest.Cancel) // cancel ongoing threads
        lastQuery = "" // reset last query
    }

    fun navigateToMovieDetailsFragment(movie: Movie) {
        navigate(
            SearchMoviesFragmentDirections.openMovieDetailsFragment(movie)
        )
    }
}
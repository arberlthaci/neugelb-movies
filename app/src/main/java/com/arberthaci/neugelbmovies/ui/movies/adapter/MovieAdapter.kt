package com.arberthaci.neugelbmovies.ui.movies.adapter

import android.util.SparseArray
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.arberthaci.neugelbmovies.common.ui.recyclerview.adapter.AdapterHelper
import com.arberthaci.neugelbmovies.common.ui.recyclerview.adapter.ViewType
import com.arberthaci.neugelbmovies.common.ui.recyclerview.adapter.ViewTypeDelegateAdapter
import com.arberthaci.neugelbmovies.data.model.Movie
import com.arberthaci.neugelbmovies.data.model.MovieDisplayMode
import com.arberthaci.neugelbmovies.ui.movies.adapter.callbacks.MovieCallback
import com.arberthaci.neugelbmovies.ui.movies.adapter.delegates.MovieGridHolderDelegate
import com.arberthaci.neugelbmovies.ui.movies.adapter.delegates.MovieLinearHolderDelegate

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

class MovieAdapter(
    private val items: ArrayList<Movie> = arrayListOf(),
    callback: MovieCallback
) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), AdapterHelper<Movie> {

    companion object {
        const val TYPE_ITEM_MOVIE_GRID = 1
        const val TYPE_ITEM_MOVIE_LINEAR = 2
    }

    private var delegateAdapters = SparseArray<ViewTypeDelegateAdapter<Movie>>()

    init {
        delegateAdapters.put(
            TYPE_ITEM_MOVIE_GRID,
            MovieGridHolderDelegate(
                callback = callback
            )
        )

        delegateAdapters.put(
            TYPE_ITEM_MOVIE_LINEAR,
            MovieLinearHolderDelegate(
                callback = callback
            )
        )
    }

    override fun getItemViewType(position: Int) = getViewType(items[position])

    override fun getViewType(item: Movie): Int {
        return when (item.displayMode) {
            MovieDisplayMode.GRID -> TYPE_ITEM_MOVIE_GRID
            MovieDisplayMode.LINEAR -> TYPE_ITEM_MOVIE_LINEAR
            else -> ViewType.TYPE_ITEM_INVALID
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        delegateAdapters[viewType].onCreateViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) =
        delegateAdapters[getItemViewType(position)].onBindViewHolder(
            holder = holder,
            item = items[position]
        )

    override fun getItemCount() = items.size

    override fun add(item: Movie) {
        if (!items.contains(item)) {
            items.add(item)
        }
        notifyItemInserted(items.size - 1)
    }

    override fun addAll(items: List<Movie>) {
        items.forEach { item ->
            add(item)
        }
    }

    override fun remove(item: Movie) {
        val position = items.indexOf(item)
        if (position > -1) {
            items.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    override fun clear() {
        val lastItemCount = itemCount
        items.clear()
        notifyItemRangeRemoved(0, lastItemCount)
    }

    override fun getItem(position: Int) = items[position]

    override fun contains(item: Movie) = items.contains(item)

    override val empty: Boolean
        get() = itemCount == 0
}
package com.arberthaci.neugelbmovies.ui.movies.view

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.viewModels
import com.arberthaci.neugelbmovies.R
import com.arberthaci.neugelbmovies.common.extensions.load
import com.arberthaci.neugelbmovies.data.model.Movie
import com.arberthaci.neugelbmovies.data.model.constants.Constants
import com.arberthaci.neugelbmovies.ui.base.BaseViewModel
import com.arberthaci.neugelbmovies.ui.base.NeugelbFragment
import com.arberthaci.neugelbmovies.ui.movies.viewmodel.MovieDetailsViewModel
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import java.text.DateFormat

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

class MovieDetailsFragment : NeugelbFragment() {

    companion object {
        const val INTENT_EXTRA_MOVIE = "MOVIE"
    }

    private var data: Movie? = null

    private lateinit var ivThumbnail: ImageView
    private lateinit var tvTitle: TextView
    private lateinit var tvReleaseDate: TextView
    private lateinit var tvPopularity: TextView
    private lateinit var tvOverview: TextView

    private val viewModel: MovieDetailsViewModel by viewModels {
        viewModelFactory
    }

    /**
     * Fragment implementation
     **/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        data = arguments?.getSerializable(INTENT_EXTRA_MOVIE) as Movie?
    }

    /**
     * NeugelbFragment implementation
     **/

    override fun getViewModel(): BaseViewModel = viewModel

    override fun getLayout(): Int = R.layout.movie_details_fragment

    override fun enableBackButton(): Boolean = true

    override fun getTitle(): Int = R.string.movie_details_toolbar_title

    override fun initializeView(view: View) {
        ivThumbnail = view.findViewById(R.id.iv_thumbnail)
        ivThumbnail.load(
            url = data?.posterPath,
            originalSize = true
        )

        tvTitle = view.findViewById(R.id.tv_title)
        tvTitle.text = data?.title

        tvReleaseDate = view.findViewById(R.id.tv_release_date)
        try {
            if (data?.releaseDate?.isNotBlank() == true) {
                val dateTime = DateTime.parse(
                    data?.releaseDate,
                    DateTimeFormat.forPattern(Constants.DATE_FORMAT)
                )
                tvReleaseDate.text = DateFormat.getDateInstance(DateFormat.LONG)
                    .format(dateTime.toDate())
            } else {
                setInvalidReleaseDate()
            }
        } catch (exception: Exception) {
            setInvalidReleaseDate()
        }

        tvPopularity = view.findViewById(R.id.tv_popularity)
        tvPopularity.text = getString(
            R.string.movie_details_popularity_placeholder,
            ((data?.voteAverage ?: 0.0) * 10).toString()
        )

        tvOverview = view.findViewById(R.id.tv_overview)
        tvOverview.text = data?.overview
    }

    /**
     * Private functions of MovieDetailsFragment
     **/

    private fun setInvalidReleaseDate() {
        tvReleaseDate.text = "-"
    }
}
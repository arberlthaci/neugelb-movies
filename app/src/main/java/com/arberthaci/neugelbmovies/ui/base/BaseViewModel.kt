package com.arberthaci.neugelbmovies.ui.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.NavDirections
import com.arberthaci.neugelbmovies.ui.base.navigation.NavigationCommand
import com.arberthaci.neugelbmovies.common.utils.Event
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

abstract class BaseViewModel : ViewModel() {

    val disposables = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }

    // FOR NAVIGATION
    private val mNavigation = MutableLiveData<Event<NavigationCommand>>()
    val navigation: LiveData<Event<NavigationCommand>> = mNavigation

    /**
     * Convenient method to handle navigation from a [ViewModel]
     */
    fun navigate(directions: NavDirections) {
        mNavigation.value = Event(NavigationCommand.To(directions))
    }
}
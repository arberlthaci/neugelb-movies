package com.arberthaci.neugelbmovies.ui.movies.viewmodel

import com.arberthaci.neugelbmovies.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

class MovieDetailsViewModel @Inject constructor() : BaseViewModel()

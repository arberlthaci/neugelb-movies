package com.arberthaci.neugelbmovies.ui.movies.adapter.callbacks

import com.arberthaci.neugelbmovies.data.model.Movie

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

interface MovieCallback {
    fun onMovieClicked(movie: Movie)
}
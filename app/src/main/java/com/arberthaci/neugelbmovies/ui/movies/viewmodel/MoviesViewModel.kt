package com.arberthaci.neugelbmovies.ui.movies.viewmodel

import androidx.lifecycle.MutableLiveData
import com.arberthaci.neugelbmovies.common.livedata.Outcome
import com.arberthaci.neugelbmovies.common.livedata.extensions.failed
import com.arberthaci.neugelbmovies.common.livedata.extensions.loading
import com.arberthaci.neugelbmovies.common.livedata.extensions.success
import com.arberthaci.neugelbmovies.common.livedata.extensions.toMutableLiveData
import com.arberthaci.neugelbmovies.data.model.Movie
import com.arberthaci.neugelbmovies.data.model.MovieDisplayMode
import com.arberthaci.neugelbmovies.data.model.RxMovieResponse
import com.arberthaci.neugelbmovies.data.repository.movies.MoviesRepository
import com.arberthaci.neugelbmovies.ui.base.BaseViewModel
import com.arberthaci.neugelbmovies.ui.movies.view.MoviesFragmentDirections
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

class MoviesViewModel @Inject constructor(
    private val moviesRepository: MoviesRepository
) : BaseViewModel() {

    private val moviesSubject: PublishSubject<Outcome<RxMovieResponse>> =
        PublishSubject.create()
    val moviesOutcome: MutableLiveData<Outcome<RxMovieResponse>> by lazy {
        moviesSubject.toMutableLiveData(disposables)
    }

    fun getLatestMovies(page: Int = 1) {
        moviesRepository.getLatestMovies(page = page)
            .map { movies ->
                movies.forEach { it.displayMode = MovieDisplayMode.GRID }

                return@map if (page == 1) {
                    RxMovieResponse.FirstPage(
                        movies = movies
                    )
                } else {
                    RxMovieResponse.NextPage(
                        movies = movies
                    )
                }
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                if (page == 1) {
                    moviesSubject.loading(true)
                }
            }
            .subscribe(
                {
                    moviesSubject.success(it)
                },
                {
                    moviesSubject.failed(it)
                }
            )
            .addTo(disposables)
    }

    fun navigateToSearchMoviesFragment() {
        navigate(
            MoviesFragmentDirections.openSearchMoviesFragment()
        )
    }

    fun navigateToMovieDetailsFragment(movie: Movie) {
        navigate(
            MoviesFragmentDirections.openMovieDetailsFragment(movie)
        )
    }
}
package com.arberthaci.neugelbmovies.ui.movies.view

import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.arberthaci.neugelbmovies.R
import com.arberthaci.neugelbmovies.common.exceptions.NeugelbException
import com.arberthaci.neugelbmovies.common.extensions.makeViewListenToTouchEvents
import com.arberthaci.neugelbmovies.common.livedata.Outcome
import com.arberthaci.neugelbmovies.common.ui.emptyview.EmptyView
import com.arberthaci.neugelbmovies.common.ui.recyclerview.EndlessScrollListener
import com.arberthaci.neugelbmovies.data.model.Movie
import com.arberthaci.neugelbmovies.data.model.RxMovieResponse
import com.arberthaci.neugelbmovies.data.repository.movies.exceptions.MoviesException
import com.arberthaci.neugelbmovies.ui.base.BaseViewModel
import com.arberthaci.neugelbmovies.ui.base.NeugelbFragment
import com.arberthaci.neugelbmovies.ui.movies.adapter.MovieAdapter
import com.arberthaci.neugelbmovies.ui.movies.adapter.callbacks.MovieCallback
import com.arberthaci.neugelbmovies.ui.movies.viewmodel.MoviesViewModel
import timber.log.Timber

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

class MoviesFragment :
    NeugelbFragment(),
    MovieCallback {

    // States
    private lateinit var clStates: ConstraintLayout
    // EmptyView
    private lateinit var evEmptyView: EmptyView
    // Results
    private lateinit var rvMovies: RecyclerView
    private lateinit var scrollListener: EndlessScrollListener
    private val movieAdapter: MovieAdapter by lazy {
        MovieAdapter(
            callback = this
        )
    }

    private val viewModel: MoviesViewModel by viewModels {
        viewModelFactory
    }

    /**
     * Fragment implementation
     **/

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_movies, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        if (item.itemId == R.id.search) {
            viewModel.navigateToSearchMoviesFragment()
            true
        } else {
            super.onOptionsItemSelected(item)
        }

    /**
     * NeugelbFragment implementation
     **/

    override fun getViewModel(): BaseViewModel = viewModel

    override fun getLayout(): Int = R.layout.movies_fragment

    override fun enableBackButton(): Boolean = false

    override fun getTitle(): Int = R.string.latest_movies_toolbar_title

    override fun initializeView(view: View) {
        view.makeViewListenToTouchEvents()

        initializeStateConstraintLayout(view = view)
        initializeEmptyView(view = view)
        initializeResultsView(view = view)

        observeViewModelGetLatestMoviesData()

        viewModel.getLatestMovies()
    }

    /**
     * MoviesCallback implementation
     **/

    override fun onMovieClicked(movie: Movie) {
        viewModel.navigateToMovieDetailsFragment(
            movie = movie
        )
    }

    /**
     * Private functions of MoviesFragment
     **/

    private fun initializeStateConstraintLayout(view: View) {
        clStates = view.findViewById(R.id.constraint_layout)
        clStates.loadLayoutDescription(R.xml.constraint_layout_movies_states)
    }

    private fun bindLoadingScreen() {
        clStates.setState(R.id.loading, 0, 0)
    }

    private fun initializeEmptyView(view: View) {
        evEmptyView = view.findViewById(R.id.empty_view)
    }

    private fun bindEmptyScreen() {
        evEmptyView.apply {
            setIcon(
                iconDrawable = R.drawable.ic_empty
            )
            setPrimaryInfo(
                text = getString(R.string.empty_screen_primary_info)
            )
            setSecondaryInfo(
                text = getString(R.string.empty_screen_secondary_info)
            )
        }
        clStates.setState(R.id.empty, 0, 0)
    }

    private fun bindErrorScreen() {
        evEmptyView.apply {
            setIcon(
                iconDrawable = R.drawable.ic_something_went_wrong
            )
            setPrimaryInfo(
                text = getString(R.string.error_screen_primary_info)
            )
            setSecondaryInfo(
                text = getString(R.string.error_screen_secondary_info)
            )
        }
        clStates.setState(R.id.empty, 0, 0)
    }

    private fun bindNoConnectionScreen() {
        evEmptyView.apply {
            setIcon(
                iconDrawable = R.drawable.ic_no_connection
            )
            setPrimaryInfo(
                text = getString(R.string.no_connection_screen_primary_info)
            )
            setSecondaryInfo(
                text = getString(R.string.no_connection_screen_secondary_info)
            )
        }
        clStates.setState(R.id.empty, 0, 0)
    }

    private fun initializeResultsView(view: View) {
        rvMovies = view.findViewById(R.id.rv_movies)

        rvMovies.apply {
            layoutManager = GridLayoutManager(activity, 2)
            itemAnimator = DefaultItemAnimator()
            scrollListener = object : EndlessScrollListener(
                layoutManager = layoutManager as GridLayoutManager
            ) {
                override fun onLoadMore(page: Int) {
                    viewModel.getLatestMovies(
                        page = page
                    )
                }
            }
            addOnScrollListener(scrollListener)
            adapter = movieAdapter
        }
    }

    private fun bindResultsScreen(
        movies: List<Movie>,
        isFirstPage: Boolean
    ) {
        if (isFirstPage) {
            resetScrollListener()
            clStates.setState(R.id.success, 0, 0)
        }
        movieAdapter.addAll(movies)
    }

    private fun resetScrollListener() {
        scrollListener.resetScrollListener(
            adapter = movieAdapter,
            recyclerView = rvMovies
        )
    }

    private fun observeViewModelGetLatestMoviesData() {
        viewModel.moviesOutcome.observe(
            this,
            Observer<Outcome<RxMovieResponse>> { outcome ->
                when (outcome) {
                    is Outcome.Progress -> {
                        if (outcome.loading) {
                            bindLoadingScreen()
                            Timber.d("GetLatestMovies loading")
                        }
                    }
                    is Outcome.Success -> {
                        when (val rxMovieResponse = outcome.data) {
                            is RxMovieResponse.FirstPage -> {
                                bindResultsScreen(
                                    movies = rxMovieResponse.movies,
                                    isFirstPage = true
                                )
                            }
                            is RxMovieResponse.NextPage -> {
                                bindResultsScreen(
                                    movies = rxMovieResponse.movies,
                                    isFirstPage = false
                                )
                            }
                        }
                        Timber.d("GetLatestMovies success ${outcome.data}")
                    }
                    is Outcome.Failure -> {
                        when (outcome.error) {
                            is MoviesException.NoResults -> {
                                bindEmptyScreen()
                            }
                            is NeugelbException.NoConnection -> {
                                bindNoConnectionScreen()
                            }
                            else -> {
                                bindErrorScreen()
                            }
                        }
                    }
                }
            }
        )
    }
}
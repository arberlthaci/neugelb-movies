package com.arberthaci.neugelbmovies.ui.movies.view

import android.view.View
import android.widget.EditText
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.arberthaci.neugelbmovies.R
import com.arberthaci.neugelbmovies.common.exceptions.NeugelbException
import com.arberthaci.neugelbmovies.common.extensions.afterTextChanged
import com.arberthaci.neugelbmovies.common.extensions.gone
import com.arberthaci.neugelbmovies.common.extensions.makeViewListenToTouchEvents
import com.arberthaci.neugelbmovies.common.extensions.visible
import com.arberthaci.neugelbmovies.common.livedata.Outcome
import com.arberthaci.neugelbmovies.common.ui.emptyview.EmptyView
import com.arberthaci.neugelbmovies.common.ui.recyclerview.EndlessScrollListener
import com.arberthaci.neugelbmovies.data.model.Movie
import com.arberthaci.neugelbmovies.data.model.RxMovieResponse
import com.arberthaci.neugelbmovies.data.repository.movies.exceptions.MoviesException
import com.arberthaci.neugelbmovies.ui.base.BaseViewModel
import com.arberthaci.neugelbmovies.ui.base.NeugelbFragment
import com.arberthaci.neugelbmovies.ui.movies.adapter.MovieAdapter
import com.arberthaci.neugelbmovies.ui.movies.adapter.callbacks.MovieCallback
import com.arberthaci.neugelbmovies.ui.movies.viewmodel.SearchMoviesViewModel
import timber.log.Timber

/**
 * Created by Arbër Thaçi on 2021-09-30.
 * Email: arberlthaci@gmail.com
 */

class SearchMoviesFragment :
    NeugelbFragment(),
    MovieCallback {

    // States
    private lateinit var clStates: ConstraintLayout
    // SearchBar
    private lateinit var etSearchBar: EditText
    private lateinit var ivSearchBarClose: ImageView
    // EmptyView
    private lateinit var evEmptyView: EmptyView
    // Results
    private lateinit var rvMovies: RecyclerView
    private lateinit var scrollListener: EndlessScrollListener
    private val movieAdapter: MovieAdapter by lazy {
        MovieAdapter(
            callback = this
        )
    }

    private val viewModel: SearchMoviesViewModel by viewModels {
        viewModelFactory
    }

    /**
     * NeugelbFragment implementation
     **/

    override fun getViewModel(): BaseViewModel = viewModel

    override fun getLayout(): Int = R.layout.search_movies_fragment

    override fun enableBackButton(): Boolean = true

    override fun getTitle(): Int? = null

    override fun initializeView(view: View) {
        view.makeViewListenToTouchEvents()

        initializeStateConstraintLayout(view = view)
        initializeSearchBar(view = view)
        initializeEmptyView(view = view)
        initializeResultsView(view = view)

        observeViewModelSearchMoviesData()
    }

    /**
     * MovieCallback implementation
     **/

    override fun onMovieClicked(movie: Movie) {
        viewModel.navigateToMovieDetailsFragment(
            movie = movie
        )
    }

    /**
     * Private functions of SearchMoviesFragment
     **/

    private fun initializeStateConstraintLayout(view: View) {
        clStates = view.findViewById(R.id.constraint_layout)
        clStates.loadLayoutDescription(R.xml.constraint_layout_search_movies_states)
    }

    private fun initializeSearchBar(view: View) {
        etSearchBar = view.findViewById(R.id.et_search_bar)
        ivSearchBarClose = view.findViewById(R.id.iv_close)

        // Observing movies
        viewModel.configureSearchMovies()

        // Adding TextChangedListener
        etSearchBar.afterTextChanged { text ->
            val trimmedQuery = text.trim().replace("\\s+".toRegex(), " ")
            when (trimmedQuery.length) {
                0 -> {
                    ivSearchBarClose.performClick()
                }
                1, 2 -> {
                    ivSearchBarClose.visible()
                    if (trimmedQuery.length < viewModel.getLastQuery().length) {
                        viewModel.resetSearchMovies()
                    }
                }
                else -> {
                    ivSearchBarClose.visible()
                    if (trimmedQuery != viewModel.getLastQuery()) {
                        viewModel.searchMovies(
                            query = trimmedQuery
                        )
                    }
                }
            }
        }

        // Setting OnClickListener for Close btn
        ivSearchBarClose.setOnClickListener {
            viewModel.resetSearchMovies()
            etSearchBar.text.clear()
            etSearchBar.clearFocus()
            ivSearchBarClose.gone()
        }

        // Showing the SearchBar
        ivSearchBarClose.gone()
    }

    private fun bindLoadingScreen() {
        clStates.setState(R.id.loading, 0, 0)
    }

    private fun initializeEmptyView(view: View) {
        evEmptyView = view.findViewById(R.id.empty_view)
        bindInitialScreen()
    }

    private fun bindInitialScreen() {
        evEmptyView.apply {
            setIcon(
                iconDrawable = R.drawable.ic_finder
            )
            setPrimaryInfo(
                text = getString(R.string.initial_screen_primary_info)
            )
            setSecondaryInfo(
                text = getString(R.string.initial_screen_secondary_info)
            )
        }
        clStates.setState(R.id.empty, 0, 0)
    }

    private fun bindEmptyScreen() {
        evEmptyView.apply {
            setIcon(
                iconDrawable = R.drawable.ic_empty
            )
            setPrimaryInfo(
                text = getString(R.string.empty_screen_primary_info)
            )
            setSecondaryInfo(
                text = getString(R.string.empty_screen_secondary_info)
            )
        }
        clStates.setState(R.id.empty, 0, 0)
    }

    private fun bindErrorScreen() {
        evEmptyView.apply {
            setIcon(
                iconDrawable = R.drawable.ic_something_went_wrong
            )
            setPrimaryInfo(
                text = getString(R.string.error_screen_primary_info)
            )
            setSecondaryInfo(
                text = getString(R.string.error_screen_secondary_info)
            )
        }
        clStates.setState(R.id.empty, 0, 0)
    }

    private fun bindNoConnectionScreen() {
        evEmptyView.apply {
            setIcon(
                iconDrawable = R.drawable.ic_no_connection
            )
            setPrimaryInfo(
                text = getString(R.string.no_connection_screen_primary_info)
            )
            setSecondaryInfo(
                text = getString(R.string.no_connection_screen_secondary_info)
            )
        }
        clStates.setState(R.id.empty, 0, 0)
    }

    private fun initializeResultsView(view: View) {
        rvMovies = view.findViewById(R.id.rv_movies)

        rvMovies.apply {
            layoutManager = LinearLayoutManager(context)
            itemAnimator = DefaultItemAnimator()
            scrollListener = object : EndlessScrollListener(
                layoutManager = layoutManager as LinearLayoutManager
            ) {
                override fun onLoadMore(page: Int) {
                    viewModel.searchMovies(
                        query = etSearchBar.text.toString(),
                        page = page
                    )
                }
            }
            addOnScrollListener(scrollListener)
            adapter = movieAdapter
        }
    }

    private fun bindResultsScreen(
        movies: List<Movie>,
        isFirstPage: Boolean
    ) {
        if (isFirstPage) {
            resetScrollListener()
            clStates.setState(R.id.success, 0, 0)
        }
        movieAdapter.addAll(movies)
    }

    private fun resetScrollListener() {
        scrollListener.resetScrollListener(
            adapter = movieAdapter,
            recyclerView = rvMovies
        )
    }

    private fun observeViewModelSearchMoviesData() {
        viewModel.searchMoviesOutcome.observe(
            this,
            Observer<Outcome<RxMovieResponse>> { outcome ->
                when (outcome) {
                    is Outcome.Progress -> {
                        if (outcome.loading) {
                            bindLoadingScreen()
                            Timber.d("SearchMovies loading")
                        }
                    }
                    is Outcome.Success -> {
                        when (val rxMovieResponse = outcome.data) {
                            is RxMovieResponse.FirstPage -> {
                                bindResultsScreen(
                                    movies = rxMovieResponse.movies,
                                    isFirstPage = true
                                )
                            }
                            is RxMovieResponse.NextPage -> {
                                bindResultsScreen(
                                    movies = rxMovieResponse.movies,
                                    isFirstPage = false
                                )
                            }
                            is RxMovieResponse.Restart -> {
                                bindInitialScreen()
                            }
                        }
                        Timber.d("SearchMovies success ${outcome.data}")
                    }
                    is Outcome.Failure -> {
                        when (outcome.error) {
                            is MoviesException.NoResults -> {
                                bindEmptyScreen()
                            }
                            is NeugelbException.NoConnection -> {
                                bindNoConnectionScreen()
                            }
                            else -> {
                                bindErrorScreen()
                            }
                        }
                    }
                }
            }
        )
    }
}
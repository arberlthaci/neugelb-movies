package com.arberthaci.neugelbmovies.ui.movies.adapter.delegates

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.arberthaci.neugelbmovies.R
import com.arberthaci.neugelbmovies.common.ui.recyclerview.adapter.ViewTypeDelegateAdapter
import com.arberthaci.neugelbmovies.data.model.Movie
import com.arberthaci.neugelbmovies.ui.movies.adapter.callbacks.MovieCallback
import com.google.android.material.card.MaterialCardView

/**
 * Created by Arbër Thaçi on 2021-09-30.
 * Email: arberlthaci@gmail.com
 */

class MovieLinearHolderDelegate(
    private val callback: MovieCallback
) : ViewTypeDelegateAdapter<Movie> {

    override fun onCreateViewHolder(parent: ViewGroup) =
        MovieLinearViewHolder(
            itemLayoutView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_movie_linear, parent, false)
        )

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: Movie) {
        holder as MovieLinearViewHolder
        holder.bind(
            item = item,
            callback = callback
        )
    }

    class MovieLinearViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {

        private val mcvRoot by lazy { itemLayoutView.findViewById<MaterialCardView>(R.id.mcv_root) }
        private val tvTitle by lazy { itemLayoutView.findViewById<TextView>(R.id.tv_title) }

        fun bind(
            item: Movie,
            callback: MovieCallback
        ) {
            if (adapterPosition % 2 == 0) {
                mcvRoot.setBackgroundResource(R.drawable.bg_gradient_left)
            } else {
                mcvRoot.setBackgroundResource(R.drawable.bg_gradient_right)
            }

            mcvRoot.setOnClickListener {
                callback.onMovieClicked(
                    movie = item
                )
            }

            tvTitle.text = item.title
        }
    }
}
package com.arberthaci.neugelbmovies.ui.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.arberthaci.neugelbmovies.R
import com.arberthaci.neugelbmovies.common.extensions.safeLet
import com.arberthaci.neugelbmovies.common.extensions.visible
import com.arberthaci.neugelbmovies.common.utils.Drawable
import com.arberthaci.neugelbmovies.ui.base.navigation.NavigationCommand
import com.google.android.material.appbar.AppBarLayout
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

abstract class BaseFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private var parentActivity: BaseActivity? = null
    private var toolbar: Toolbar? = null
    private var appbar: AppBarLayout? = null

    abstract fun getViewModel(): BaseViewModel

    abstract fun getLayout(): Int
    abstract fun enableBackButton(): Boolean
    @StringRes
    abstract fun getTitle(): Int?
    abstract fun initializeView(view: View)

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)

        if (context is BaseActivity) {
            val activity = context as BaseActivity?
            this.parentActivity = activity
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeNavigation(getViewModel())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(getLayout(), container, false)
        setUpAppBar(view)
        initializeView(view)
        return view
    }

    override fun onResume() {
        super.onResume()
        setSupportActionBar()
    }

    private fun setUpAppBar(view: View) {
        appbar = view.findViewById(R.id.base_toolbar)
        toolbar = view.findViewById(R.id.toolbar)

        val toolbarTitle = getTitle()?.let { mTitleResId ->
            getString(mTitleResId)
        } ?: ""

        appbar?.visible()
        toolbar?.apply {
            setHasOptionsMenu(true)
            if (enableBackButton()) {
                navigationIcon = Drawable.tint(
                    context = context,
                    drawableRes = R.drawable.ic_baseline_arrow_back_24,
                    colorInt = R.color.grey_700
                )
                setNavigationOnClickListener {
                    findNavController().navigateUp()
                }
            }
            title = toolbarTitle
        }
    }

    private fun setSupportActionBar() {
        safeLet(activity as? AppCompatActivity, toolbar) { mActivity, mToolbar ->
            mActivity.setSupportActionBar(mToolbar)
        }
    }

    fun getAppBar(): AppBarLayout? = appbar

    /**
     * Observe a [NavigationCommand] [Event] [LiveData].
     * When this [LiveData] is updated, [Fragment] will navigate to its destination
     */
    private fun observeNavigation(viewModel: BaseViewModel) {
        viewModel.navigation.observe(viewLifecycleOwner, Observer { event ->
            event?.getContentIfNotHandled()?.let { command ->
                when (command) {
                    is NavigationCommand.To -> {
                        findNavController().navigate(command.directions)
                    }
                    is NavigationCommand.Back -> {
                        findNavController().navigateUp()
                    }
                }
            }
        })
    }
}
package com.arberthaci.neugelbmovies.ui.movies.di

import com.arberthaci.neugelbmovies.ui.movies.view.MovieDetailsFragment
import com.arberthaci.neugelbmovies.ui.movies.view.MoviesFragment
import com.arberthaci.neugelbmovies.ui.movies.view.SearchMoviesFragment
import dagger.Binds
import dagger.Module
import dagger.Subcomponent
import dagger.android.AndroidInjector
import dagger.multibindings.ClassKey
import dagger.multibindings.IntoMap

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

@Subcomponent
interface MoviesFragmentSubcomponent : AndroidInjector<MoviesFragment> {
    @Subcomponent.Factory
    abstract class Factory : AndroidInjector.Factory<MoviesFragment>
}

@Subcomponent
interface SearchMoviesFragmentSubcomponent : AndroidInjector<SearchMoviesFragment> {
    @Subcomponent.Factory
    abstract class Factory : AndroidInjector.Factory<SearchMoviesFragment>
}

@Subcomponent
interface MovieDetailsFragmentSubcomponent : AndroidInjector<MovieDetailsFragment> {
    @Subcomponent.Factory
    abstract class Factory : AndroidInjector.Factory<MovieDetailsFragment>
}

@Module(
    subcomponents = [
        (MoviesFragmentSubcomponent::class),
        (SearchMoviesFragmentSubcomponent::class),
        (MovieDetailsFragmentSubcomponent::class)
    ]
)
abstract class MoviesFragmentModule {
    @Binds
    @IntoMap
    @ClassKey(MoviesFragment::class)
    abstract fun bindsMoviesFragmentModuleInjectorFactory(
        builder: MoviesFragmentSubcomponent.Factory
    ): AndroidInjector.Factory<*>

    @Binds
    @IntoMap
    @ClassKey(SearchMoviesFragment::class)
    abstract fun bindsSearchMoviesFragmentModuleInjectorFactory(
        builder: SearchMoviesFragmentSubcomponent.Factory
    ): AndroidInjector.Factory<*>

    @Binds
    @IntoMap
    @ClassKey(MovieDetailsFragment::class)
    abstract fun bindsMovieDetailsFragmentModuleInjectorFactory(
        builder: MovieDetailsFragmentSubcomponent.Factory
    ): AndroidInjector.Factory<*>
}
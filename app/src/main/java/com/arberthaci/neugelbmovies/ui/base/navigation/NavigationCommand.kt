package com.arberthaci.neugelbmovies.ui.base.navigation

import androidx.lifecycle.ViewModel
import androidx.navigation.NavDirections

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 *
 * * * * * * * * * * * * * * * * * * * *
 *
 * A simple sealed class to handle more properly
 * navigation from a [ViewModel]
 */

sealed class NavigationCommand {
    data class To(val directions: NavDirections) : NavigationCommand()
    object Back : NavigationCommand()
}
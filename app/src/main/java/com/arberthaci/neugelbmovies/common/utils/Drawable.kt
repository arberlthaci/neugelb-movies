package com.arberthaci.neugelbmovies.common.utils

import android.content.Context
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.graphics.drawable.Drawable
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import com.arberthaci.neugelbmovies.common.extensions.safeLet

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 *
 * * * * * * * * * * * * * * * * * * * *
 *
 * Returns a tinted [Drawable]
 * @param context The context of the view
 * @param drawableRes The resId of the Drawable
 * @param colorInt The color for tinting this Drawable
 */

object Drawable {
    fun tint(
        context: Context?,
        @DrawableRes drawableRes: Int,
        colorInt: Int?
    ): Drawable? {
        val drawableObject = context?.let { ContextCompat.getDrawable(it, drawableRes) }
        safeLet(drawableObject, colorInt) { drawable, color ->
            drawable.colorFilter = PorterDuffColorFilter(color, PorterDuff.Mode.SRC_ATOP)
        }
        return drawableObject
    }
}
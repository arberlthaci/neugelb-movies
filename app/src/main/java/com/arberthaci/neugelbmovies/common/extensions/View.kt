package com.arberthaci.neugelbmovies.common.extensions

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.core.view.isGone
import androidx.core.view.isInvisible
import androidx.core.view.isVisible

/**
 * Created by Arbër Thaçi on 2021-09-30.
 * Email: arberlthaci@gmail.com
 */

/**
 * Makes the view VISIBLE
 */
fun View.visible() {
    this.let {
        if (!it.isVisible) {
            it.isVisible = true
        }
    }
}

/**
 * Makes the view INVISIBLE
 */
fun View.invisible() {
    this.let {
        if (!it.isInvisible) {
            it.isInvisible = true
        }
    }
}

/**
 * Makes the view GONE
 */
fun View.gone() {
    this.let {
        if (!it.isGone) {
            it.isGone = true
        }
    }
}

/**
 * Makes keyboard HIDE
 */
fun View.hideKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
}

/**
 * If the user touches the screen outside an EditText,
 * the keyboard will automatically be closed.
 */
@SuppressLint("ClickableViewAccessibility")
fun View.makeViewListenToTouchEvents() {
    if (this !is EditText) {
        this.setOnTouchListener { _, _ ->
            hideKeyboard()
            false
        }
    }

    if (this is ViewGroup) {
        for (i in 0..this.childCount) {
            this.getChildAt(i)?.makeViewListenToTouchEvents()
        }
    }
}
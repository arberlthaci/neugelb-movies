package com.arberthaci.neugelbmovies.common.extensions

import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.arberthaci.neugelbmovies.R
import com.arberthaci.neugelbmovies.data.model.constants.Constants
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

fun ImageView.load(
    url: String?,
    originalSize: Boolean,
    onError: (() -> Unit) = {}
) {
    url?.let { mUrl ->
        Glide.with(this.context)
            .load(Constants.IMAGE_BASE_URL +
                    if (originalSize) {
                        Constants.IMAGE_FILE_SIZE_ORIGINAL
                    } else {
                        Constants.IMAGE_FILE_SIZE_W500
                    } + mUrl
            )
            .centerCrop()
            .addListener(object : RequestListener<Drawable> {
                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    onError.invoke()
                    return false
                }
            })
            .placeholder(R.drawable.ic_placeholder_loading)
            .error(R.drawable.ic_placeholder_error)
            .fallback(R.drawable.ic_placeholder_error)
            .into(this)
    } ?: run {
        this.setImageResource(R.drawable.ic_placeholder_error)
        onError.invoke()
    }
}
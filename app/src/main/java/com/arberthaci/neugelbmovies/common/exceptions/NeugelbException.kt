package com.arberthaci.neugelbmovies.common.exceptions

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 *
 * * * * * * * * * * * * * * * * * * * *
 *
 * Base Class for handling errors/failures/exceptions.
 * Every feature specific failure should extend [FeatureException] class.
 */

sealed class NeugelbException : RuntimeException() {

    object NoConnection : NeugelbException()
    object ServerError : NeugelbException()

    /**
     * Extend this class for feature specific failures.
     **/
    abstract class FeatureException : NeugelbException()
}
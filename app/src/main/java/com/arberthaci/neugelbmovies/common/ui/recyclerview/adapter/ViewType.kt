package com.arberthaci.neugelbmovies.common.ui.recyclerview.adapter

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

interface ViewType<T> {

    companion object {
        const val TYPE_ITEM_INVALID = -1
    }

    fun getViewType(item: T): Int = TYPE_ITEM_INVALID
}

package com.arberthaci.neugelbmovies.common.livedata.extensions

import androidx.lifecycle.MutableLiveData
import com.arberthaci.neugelbmovies.common.livedata.Outcome
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

/**
 * Extension function to convert a Publish subject into a MutableLiveData by subscribing to it.
 **/
fun <T> PublishSubject<T>.toMutableLiveData(compositeDisposable: CompositeDisposable): MutableLiveData<T> {
    val data = MutableLiveData<T>()
    compositeDisposable.add(this.subscribe { t: T -> data.value = t })
    return data
}

/**
 * Extension function to push a failed event with an exception to the observing outcome
 **/
fun <T> PublishSubject<Outcome<T>>.failed(e: Throwable) {
    with(this) {
        loading(false)
        onNext(Outcome.failure(e))
    }
}

/**
 * Extension function to push a success event with data to the observing outcome
 **/
fun <T> PublishSubject<Outcome<T>>.success(t: T) {
    with(this) {
        loading(false)
        onNext(Outcome.success(t))
    }
}

/**
 * Extension function to push the loading status to the observing outcome
 **/
fun <T> PublishSubject<Outcome<T>>.loading(isLoading: Boolean) {
    this.onNext(Outcome.loading(isLoading))
}
package com.arberthaci.neugelbmovies.common.extensions

import com.arberthaci.neugelbmovies.common.exceptions.NeugelbException
import io.reactivex.Observable

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

fun <T> Observable<T>.swapException(exception: NeugelbException): Observable<T> {
    return this.onErrorResumeNext { it: Throwable ->
        // do not remove type for lambda, it's rxjava fix
        if (it is NeugelbException) {
            Observable.error(it)
        } else {
            Observable.error(exception)
        }
    }
}
package com.arberthaci.neugelbmovies.common.utils

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

object InternetUtils {

    private lateinit var application: Application

    fun init(application: Application) {
        this.application = application
    }

    fun isInternetOn(): Boolean {
        val cm = application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }
}
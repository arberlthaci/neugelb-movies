package com.arberthaci.neugelbmovies.common.ui.recyclerview.adapter

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

interface AdapterHelper<T> : ViewType<T> {

    fun add(item: T)

    fun addAll(items: List<T>)

    fun remove(item: T)

    fun clear()

    fun getItem(position: Int): T

    fun contains(item: T): Boolean

    val empty: Boolean
}
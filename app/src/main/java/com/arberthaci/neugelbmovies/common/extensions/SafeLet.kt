package com.arberthaci.neugelbmovies.common.extensions

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 *
 * * * * * * * * * * * * * * * * * * * *
 *
 * Chain multiple lets for multiple nullable variables in Kotlin
 */

fun <T1 : Any, T2 : Any, R : Any> safeLet(p1: T1?, p2: T2?, block: (T1, T2) -> R?): R? {
    return if (p1 != null && p2 != null) block(p1, p2) else null
}
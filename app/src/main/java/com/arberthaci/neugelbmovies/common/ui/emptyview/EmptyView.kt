package com.arberthaci.neugelbmovies.common.ui.emptyview

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.arberthaci.neugelbmovies.R

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

class EmptyView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    private val ivIcon: ImageView
    private val tvPrimaryInfo: TextView
    private val tvSecondaryInfo: TextView

    init {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.empty_view, this, true)

        ivIcon = view.findViewById(R.id.iv_empty_view_icon)
        tvPrimaryInfo = view.findViewById(R.id.tv_empty_view_primary_info)
        tvSecondaryInfo = view.findViewById(R.id.tv_empty_view_secondary_info)
    }

    fun setIcon(iconDrawable: Int) {
        ivIcon.setImageDrawable(ContextCompat.getDrawable(context, iconDrawable))
    }

    fun setPrimaryInfo(text: String) {
        tvPrimaryInfo.text = text
    }

    fun setSecondaryInfo(text: String) {
        tvSecondaryInfo.text = text
    }
}
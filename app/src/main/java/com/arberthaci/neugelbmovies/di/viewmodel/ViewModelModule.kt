package com.arberthaci.neugelbmovies.di.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.arberthaci.neugelbmovies.ui.movies.viewmodel.MovieDetailsViewModel
import com.arberthaci.neugelbmovies.ui.movies.viewmodel.MoviesViewModel
import com.arberthaci.neugelbmovies.ui.movies.viewmodel.SearchMoviesViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(
        factory: ViewModelFactory
    ): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MoviesViewModel::class)
    abstract fun bindsMoviesViewModel(
        viewModel: MoviesViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SearchMoviesViewModel::class)
    abstract fun bindsSearchMoviesViewModel(
        viewModel: SearchMoviesViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MovieDetailsViewModel::class)
    abstract fun bindsMovieDetailsViewModel(
        viewModel: MovieDetailsViewModel
    ): ViewModel
}
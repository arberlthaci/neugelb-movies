package com.arberthaci.neugelbmovies.di.module

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

@Module
class AppModule {

    @Provides
    @Singleton
    internal fun providesContext(
        application: Application
    ): Context = application
}
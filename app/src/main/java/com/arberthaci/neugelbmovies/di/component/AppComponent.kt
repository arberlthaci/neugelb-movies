package com.arberthaci.neugelbmovies.di.component

import android.app.Application
import com.arberthaci.neugelbmovies.NeugelbApplication
import com.arberthaci.neugelbmovies.di.module.AppBindingModule
import com.arberthaci.neugelbmovies.di.module.AppModule
import com.arberthaci.neugelbmovies.di.module.RemoteModule
import com.arberthaci.neugelbmovies.di.module.RepositoryModule
import com.arberthaci.neugelbmovies.di.viewmodel.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppBindingModule::class,
        AppModule::class,
        RemoteModule::class,
        RepositoryModule::class,
        ViewModelModule::class
    ]
)
interface AppComponent {
    fun inject(application: NeugelbApplication)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}
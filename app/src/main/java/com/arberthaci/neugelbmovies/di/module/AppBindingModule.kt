package com.arberthaci.neugelbmovies.di.module

import com.arberthaci.neugelbmovies.MainActivity
import com.arberthaci.neugelbmovies.ui.movies.di.MoviesFragmentModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

@Module
abstract class AppBindingModule {
    @ContributesAndroidInjector(
        modules = [
            (MoviesFragmentModule::class)
        ]
    )
    abstract fun bindMainActivity(): MainActivity
}
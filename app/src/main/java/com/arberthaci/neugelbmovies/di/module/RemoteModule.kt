package com.arberthaci.neugelbmovies.di.module

import com.arberthaci.neugelbmovies.data.remote.NeugelbNetwork
import com.arberthaci.neugelbmovies.data.remote.Network
import com.arberthaci.neugelbmovies.data.remote.movies.MoviesApiHelper
import com.arberthaci.neugelbmovies.data.remote.movies.MoviesApiHelperImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

@Module
class RemoteModule {

    @Provides
    @Singleton
    fun providesNetwork(): Network = NeugelbNetwork()

    @Provides
    @Singleton
    fun providesMoviesApiHelper(
        network: Network
    ): MoviesApiHelper = MoviesApiHelperImpl(
        network = network
    )
}
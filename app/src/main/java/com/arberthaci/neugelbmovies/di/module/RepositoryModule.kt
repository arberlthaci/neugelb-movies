package com.arberthaci.neugelbmovies.di.module

import com.arberthaci.neugelbmovies.data.remote.movies.MoviesApiHelper
import com.arberthaci.neugelbmovies.data.repository.movies.MoviesRepository
import com.arberthaci.neugelbmovies.data.repository.movies.MoviesRepositoryImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun providesMoviesRepository(
        apiHelper: MoviesApiHelper
    ): MoviesRepository = MoviesRepositoryImpl(
        apiHelper = apiHelper
    )
}
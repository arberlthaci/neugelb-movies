/**
 * Created by Arbër Thaçi on 2021-09-29.
 * Email: arberlthaci@gmail.com
 */

object ApplicationId {
    const val id = "com.arberthaci.neugelbmovies"
}

object Releases {
    const val versionCode = 1
    const val versionName = "1.0.0"
}

object Versions {
    const val kotlin = "1.4.31"
    const val gradle = "7.0.2"
    const val compileSdk = 30
    const val minSdk = 23
    const val targetSdk = 30
    const val appCompat = "1.2.0"
    const val coreKtx = "1.3.2"
    const val constraintLayout = "2.0.4"
    const val retrofit = "2.6.1"
    const val gson = "2.8.7"
    const val okHttp = "3.12.1"
    const val rxjavaCore = "2.2.12"
    const val rxjavaKotlinVersion = "2.4.0"
    const val rxjavaAndroidVersion = "2.1.1"
    const val dagger = "2.29.1"
    const val timber = "4.7.1"
    const val jodaTime = "2.9.9"
    const val lifecycle = "2.3.0"
    const val lifecycleExtensions = "2.2.0"
    const val recyclerview = "1.0.0"
    const val navigationSafeArgs = "2.3.0"
    const val glide = "4.11.0"
    const val material = "1.3.0"
}

object Libraries {
    // DAGGER
    const val dagger = "com.google.dagger:dagger:${Versions.dagger}"
    const val daggerCompiler = "com.google.dagger:dagger-compiler:${Versions.dagger}"
    const val daggerJavaXAnnotation =  "javax.annotation:jsr250-api:1.0"
    // RXJAVA
    const val rxjava = "io.reactivex.rxjava2:rxjava:${Versions.rxjavaCore}"
    // RETROFIT
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val retrofitGsonConverter = "com.squareup.retrofit2:converter-gson:${Versions.retrofit}"
    const val retrofitRxJavaAdapter = "com.squareup.retrofit2:adapter-rxjava2:${Versions.retrofit}"
    const val httpLoggingInterceptor = "com.squareup.okhttp3:logging-interceptor:${Versions.okHttp}"
    const val gson = "com.google.code.gson:gson:${Versions.gson}"
    // GLIDE
    const val glide = "com.github.bumptech.glide:glide:${Versions.glide}"
    // TIMBER
    const val timber = "com.jakewharton.timber:timber:${Versions.timber}"
    // Joda Time
    const val jodaTime = "net.danlew:android.joda:${Versions.jodaTime}"
}

object KotlinLibraries {
    const val kotlin = "org.jetbrains.kotlin:kotlin-stdlib:${Versions.kotlin}"
    const val rxjava = "io.reactivex.rxjava2:rxkotlin:${Versions.rxjavaKotlinVersion}"
}

object AndroidLibraries {
    // ANDROID
    const val appCompat = "androidx.appcompat:appcompat:${Versions.appCompat}"
    const val coreKtx = "androidx.core:core-ktx:${Versions.coreKtx}"
    const val constraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
    const val lifecycleViewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle}"
    const val lifecycleExtensions = "androidx.lifecycle:lifecycle-extensions:${Versions.lifecycleExtensions}"
    const val recyclerView = "androidx.recyclerview:recyclerview:${Versions.recyclerview}"
    const val navigation = "androidx.navigation:navigation-ui-ktx:${Versions.navigationSafeArgs}"
    const val navigationFrag = "androidx.navigation:navigation-fragment-ktx:${Versions.navigationSafeArgs}"
    // MATERIAL
    const val material = "com.google.android.material:material:${Versions.material}"
    // DAGGER
    const val daggerProcessor = "com.google.dagger:dagger-android-processor:${Versions.dagger}"
    const val daggerSupport = "com.google.dagger:dagger-android-support:${Versions.dagger}"
    // RXJAVA
    const val rxjava = "io.reactivex.rxjava2:rxandroid:${Versions.rxjavaAndroidVersion}"
}

object TestLibraries {
    const val junit = "junit:junit:4.13-beta-3"
    const val testCore = "androidx.arch.core:core-testing:2.0.0"
    const val mockk = "io.mockk:mockk:1.9.3"
}